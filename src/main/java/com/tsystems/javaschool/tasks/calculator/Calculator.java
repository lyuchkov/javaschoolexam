package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            //parse and save to array list
            List<String> list = new ArrayList<>();
            char c;
            int p = 0;
            while (p < statement.length()) {
                c = statement.charAt(p);
                if (c == '(' | c == ')' | c == '+' | c == '-' | c == '*' | c == '/') {
                    list.add(String.valueOf(c));
                    p++;
                } else if (c >= '0' && c <= '9') {
                    StringBuilder sb = new StringBuilder();
                    boolean d;
                    do {
                        d=false;
                        sb.append(c);
                        p++;
                        if (p >= statement.length()) {
                            break;
                        }
                        c = statement.charAt(p);
                        if(c=='.'
                                &&(statement.charAt(p-1)<= '9'&&statement.charAt(p-1) >= '0')
                                &&(statement.charAt(p+1)<= '9'&&statement.charAt(p+1) >= '0')) d = true;
                    } while ((c <= '9' && c >= '0')|d);
                    list.add(sb.toString());
                } else {
                    if (c != ' ') {
                        return null;
                    }
                    p++;
                }
            }
            //check the number of brackets
            int i = 0;
            for (String s:
                    list) {
                if(s.equals("(")) i++;
                if(s.equals(")")) i--;
            }
            if(i!=0) return null;
            //iterate list
            Stack<Double> numbers = new Stack<>();
            Stack<Character> operations = new Stack<>();
            int position = 0;
            Runnable sum = () -> {
                operations.pop();
                double x = numbers.pop();
                double y = numbers.pop();
                numbers.push(x + y);
            };
            Runnable sub = () -> {
                operations.pop();
                double x = numbers.pop();
                double y = numbers.pop();
                numbers.push(y - x);
            };
            Runnable multipl = () -> {
                operations.pop();
                double x = numbers.pop();
                double y = numbers.pop();
                numbers.push(x * y);
            };
            Runnable div = () -> {
                operations.pop();
                double x = numbers.pop();
                double y = numbers.pop();
                numbers.push(y / x);
            };
            while (position < list.size()) {
                String s = list.get(position);
                if (s.matches("\\d+\\.\\d+") || s.matches("\\d+")) {
                    numbers.push(Double.valueOf(s));
                    position++;
                    continue;
                }
                if (s.equals("+")) {
                    if (operations.empty()) {
                        operations.push('+');
                        position++;
                        continue;
                    } else {
                        if (operations.peek().equals('+')) {
                            sum.run();
                            operations.push('+');
                        } else if (operations.peek().equals('-')) {
                            sub.run();
                            operations.push('+');
                        } else if (operations.peek().equals('*')) {
                            multipl.run();
                            operations.push('+');
                        } else if (operations.peek().equals('/')) {
                            div.run();
                            operations.push('+');
                        } else {
                            operations.push('+');
                        }
                    }
                    position++;
                    continue;
                }
                if (s.equals("-")) {
                    if (operations.empty()) {
                        operations.push('-');
                        position++;
                        continue;
                    } else {
                        if (operations.peek().equals('+')) {
                            sum.run();
                            operations.push('-');
                        } else if (operations.peek().equals('-')) {
                            sub.run();
                            operations.push('-');
                        } else if (operations.peek().equals('*')) {
                            multipl.run();
                            operations.push('-');
                        } else if (operations.peek().equals('/')) {
                            div.run();
                            operations.push('-');
                        } else {
                            operations.push('-');
                        }
                    }
                    position++;
                    continue;
                }
                if (s.equals("*")) {
                    if (operations.empty()) {
                        operations.push('*');
                        position++;
                        continue;
                    } else {
                        if (operations.peek().equals('(')) {
                            operations.push('*');
                        } else if (operations.peek().equals('+') || operations.peek().equals('-')) {
                            position++;
                            double x = Integer.parseInt(list.get(position));
                            double y = numbers.pop();
                            numbers.push(x * y);
                        } else if (operations.peek().equals('*')) {
                            multipl.run();
                            operations.push('*');
                        } else if (operations.peek().equals('/')) {
                            div.run();
                            operations.push('*');
                        }
                    }
                    position++;
                    continue;
                }
                if (s.equals("/")) {
                    if (operations.empty()) {
                        operations.push('/');
                    } else {
                        if (operations.peek().equals('(')) operations.push('/');
                        else if (operations.peek().equals('+') || operations.peek().equals('-')) {
                            position++;
                            double x = Integer.parseInt(list.get(position));
                            double y = numbers.pop();
                            numbers.push(y / x);
                        } else if (operations.peek().equals('*')) {
                            multipl.run();
                            operations.push('/');
                        } else if (operations.peek().equals('/')) {
                            div.run();
                            operations.push('/');
                        }
                    }
                    position++;
                    continue;
                }
                if (s.equals("(")) {
                    operations.push('(');
                    position++;
                    continue;
                }
                if (s.equals(")")) {
                    while (true) {
                        if (operations.peek() == '(') {
                            operations.pop();
                            break;
                        } else if (operations.peek() == '+') {
                            sum.run();
                        } else if (operations.peek() == '-') {
                            sub.run();
                        } else if (operations.peek() == '*') {
                            multipl.run();
                        } else if (operations.peek() == '/') {
                            div.run();
                        }
                    }
                    position++;
                }

            }
            double res = 0;
            if (operations.peek() == '+') res = numbers.pop() + numbers.pop();
            else if (operations.peek() == '-') {
                double x = numbers.pop();
                double y = numbers.pop();
                res = y - x;
            } else if (operations.peek() == '*') res = numbers.pop() * numbers.pop();
            else if (operations.peek() == '/') {
                double x = numbers.pop();
                double y = numbers.pop();
                res =  (y / x);
            }
            if(Double.isInfinite(res)) return null;
            if(res%1==0) return String.valueOf((int)res);
            else return String.valueOf(res);
        }catch (NullPointerException | ArithmeticException | EmptyStackException e){
            return null;
        }
    }
}
