package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try{
            int[][] pyramid;

            int numCount = 0;
            int rows = 1;
            int cols = 1;
            while (numCount < inputNumbers.size()) {
                numCount = numCount + rows;
                rows++;
                cols = cols + 2;
            }
            rows = rows - 1;
            cols = cols - 2;

            if (inputNumbers.size() == numCount) {
                Collections.sort(inputNumbers);
                pyramid = new int[rows][cols];

                int center = (cols / 2);
                numCount = 1;
                int arrIdx = 0;

                for (int i = 0, shift = 0; i < rows; i++, shift++, numCount++) {
                    int start = center - shift;
                    for (int j = 0; j < numCount * 2; j += 2, arrIdx++) {
                        pyramid[i][start + j] = inputNumbers.get(arrIdx);
                    }
                }
            } else {
                throw new CannotBuildPyramidException();
            }
            return pyramid;
        }
        catch (OutOfMemoryError | NullPointerException outOfMemoryError) {
            throw new CannotBuildPyramidException();
        }
    }
}
