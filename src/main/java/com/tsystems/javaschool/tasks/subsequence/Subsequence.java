package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        try {
            if (x.size() > y.size()) return false;
            else if (x.isEmpty() || (x.isEmpty()&&y.isEmpty())) return true;
            else {
                int index = -1;
                for (Object o : x) {
                    if (index < y.indexOf(o)) index = y.indexOf(o);
                    else return false;
                }
                return true;
            }
        }catch (NullPointerException e){
            throw new IllegalArgumentException();
        }
    }
}
